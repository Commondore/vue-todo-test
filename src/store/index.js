import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const TASKS = "tasks";

const store = new Vuex.Store({
  state: {
    tasks: [],
  },
  mutations: {
    addTask(state, task) {
      state.tasks.push(task);
      localStorage.setItem(TASKS, JSON.stringify(state.tasks));
    },
    remove(state, title) {
      state.tasks = state.tasks.filter((item) => item.title !== title);
      localStorage.setItem(TASKS, JSON.stringify(state.tasks));
    },
    init(state) {
      const tasks = JSON.parse(localStorage.getItem(TASKS)) || [];
      state.tasks = tasks;
    },
    update(state, data) {
      const index = state.tasks.findIndex((item) => item.title === data.title);
      state.tasks[index].title = data.update;
      localStorage.setItem(TASKS, JSON.stringify(state.tasks));
    },
  },
});

export default store;
